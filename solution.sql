SELECT customerName, country 
FROM customers 
WHERE country LIKE "%Philippines%";


SELECT customerName, contactLastName, contactFirstName
FROM customers
WHERE customerName LIKE "%La Rochelle Gifts%";


SELECT productName, MSRP
FROM products
WHERE productName = "The Titanic";


SELECT lastName, firstName, email
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";


SELECT customerName, state
FROM customers
WHERE state IS NULL;


SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson"
AND firstName = "Steve";


SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA"
AND creditLimit > 3000;


SELECT customerName
FROM customers
WHERE customerName NOT LIKE "%a%";


SELECT customerNumber, comments
FROM orders
WHERE comments LIKE "%DHL%";


SELECT productLine
FROM productLines
WHERE textDescription LIKE "%state of the art%";


SELECT DISTINCT country
FROM customers;


SELECT DISTINCT status
FROM orders;


SELECT customerName, country
FROM customers
WHERE country IN ("USA", "France", "Canada");


SELECT 
	employees.firstName, employees.lastName, offices.city
FROM offices
JOIN employees
	ON offices.officeCode = employees.officeCode
WHERE
	offices.city = "Tokyo";


SELECT 
	c.customerName, 
	CONCAT(e.firstName, " ", e.lastName) AS "SalesRep Name"
FROM employees e
JOIN customers c
	ON e.employeeNumber = c.salesRepEmployeeNumber
WHERE e.firstName = "Leslie"
AND e.lastName = "Thompson";


SELECT products.productName, customers.customerName
FROM customers
JOIN orders
	ON customers.customerNumber = orders.customerNumber
JOIN orderdetails
	ON orders.orderNumber = orderdetails.orderNumber
JOIN products
	ON orderdetails.productCode = products.productCode
WHERE customerName = "Baane Mini Imports";


SELECT
	employees.firstName AS "SalesRep firstName",
	employees.lastName AS "SalesRep lastName",
	customers.customerName,
	customers.country AS "country"
FROM customers
JOIN employees
	ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices
	ON employees.officeCode = offices.OfficeCode
WHERE customers.country = offices.country;


SELECT
	e.lastName,
	e.firstName,
	CONCAT (s.firstName, " ", s.lastName) AS "Supervisor"
FROM employees e
JOIN employees s
	ON e.reportsTo = s.employeeNumber
WHERE 
	s.firstName = "Anthony"
	AND s.lastName = "Bow";


SELECT productName, MSRP
FROM products
WHERE MSRP = (SELECT MAX(MSRP) FROM products);


SELECT COUNT(customerName) AS "Number of Customers in UK"
FROM customers
WHERE country = "UK";


SELECT 
	productLine,
	COUNT(productname) AS "Number of products"
FROM products
GROUP BY productLine;


SELECT 
	CONCAT (e.lastName, ", ", e.firstName) AS "Employee Name",
	COUNT(c.customerName)
FROM customers c
JOIN employees e
	ON c.salesRepEmployeeNumber = e.employeeNumber
GROUP BY e.employeeNumber
ORDER BY e.lastName ASC;


SELECT productName, quantityInStock, productLine
FROM products
WHERE productLine = "Planes"
AND quantityInStock < 1000;